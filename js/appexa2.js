
/*
morigen=document.getElementById('morigen');

$(morigen).ready(function(){
    $('select').on('change', function(event ) {
        //Reactivar valor previmante seleccionado
        var prevValue = $(this).data('previous');
        $('select').not(this).find('option[value="'+prevValue+'"]').show();
        //Ocultar opción seleccionada              
        var value = $(this).val();
        // Actualizar datos anteriormente seleccionados
        $(this).data('previous',value);
        $('select').not(this).find('option[value="'+value+'"]').hide();
    });
 }); */

 // Evento calcular

 function calcular(){
    var can=document.getElementById('cantidad').value;
    var ori=document.getElementById('morigen').value;
    var des=document.getElementById('mdestino').value;
    let subtotal=0.0;

    // <option value="1">Pesos Mexicanos</option>
    //<option value="2">Dólar Americano</option>
    //<option value="3">Dólar Canadiense</option>
    //<option value="4">Euro</option>

    // pesos mexicanos a
    if(ori==1 && des==2){
        subtotal=can/(19.85);
    } else if(ori==1 && des==3){
        subtotal=(can/(19.85))*1.35;
    } else if(ori==1 && des==4){
        subtotal=(can/(19.85))*0.99;
    } else if(ori==2 && des==1){ // dolar americano a
        subtotal=can*19.85;
    } else if(ori==2 && des==3){
        subtotal=can*1.35;
    } else if(ori==2 && des==4){
        subtotal=can*.99;
    } else if(ori==3 && des==1){  // dolar canadiense a
        subtotal=(can/(1.3))*19.85;
    } else if(ori==3 && des==2){
        subtotal=can/(1.3);
    } else if(ori==3 && des==4){
        subtotal=(can/(1.3))*0.99;
    } else if(ori==4 && des==1){ // euro a
        subtotal=(can/(0.99))*19.85;
    } else if(ori==4 && des==2){
        subtotal=(can/(0.99));
    } else if(ori==4 && des==3){
        subtotal=(can/(0.99))*1.35;
    } else{
        subtotal=0; 
    }

    // console.log("Cantidad "+can+" Origen "+ori+" Destino "+des+" Subtotal: "+subtotal);
    document.getElementById("subtotal").value = subtotal.toFixed(2);
    document.getElementById("comision").value = (subtotal*0.03).toFixed(2);
    document.getElementById("total").value = (subtotal-(subtotal*0.03)).toFixed(2); 

    return subtotal;
 }

 function registrar(subtotal){
    var subtotal1=document.getElementById('subtotal1');
    var comision1=document.getElementById('comision1');
    var total1=document.getElementById('total1');

    subtotal1.innerHTML=subtotal.toFixed(2);
    comision1.innerHTML=(subtotal*0.03).toFixed(2);
    total1.innerHTML=(subtotal-(subtotal*0.03)).toFixed(2); 
 }

 function borrar(){
    var subtotal1=document.getElementById('subtotal1');
    var comision1=document.getElementById('comision1');
    var total1=document.getElementById('total1');

    subtotal1.innerHTML="";
    comision1.innerHTML="";
    total1.innerHTML="";
}